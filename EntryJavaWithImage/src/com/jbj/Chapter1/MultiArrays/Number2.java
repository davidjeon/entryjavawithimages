package com.jbj.Chapter1.MultiArrays;

public class Number2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[][] a = { { 10, 20, 30 }, { 40, 50 }, { 60, 70, 80, 90 } };

		System.out.println("일차원 배열의 요소수 " + a.length);
		System.out.println("a[0]의 요소수 " + a[0].length);
		System.out.println("a[1]의 요소수 " + a[1].length);
		System.out.println("a[2]의 요소수 " + a[2].length);

	}

}
