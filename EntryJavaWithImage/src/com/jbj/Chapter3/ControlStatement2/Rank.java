package com.jbj.Chapter3.ControlStatement2;

public class Rank {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num = 2;
		System.out.println(num);

		if (0 <= num && num <= 9) {
			System.out.println("1st digit");
		} else if (10 <= num && num <= 99) {
			System.out.println("2nd digit");
		} else if (100 <= num && num <= 999) {
			System.out.println("3rd digit");
		} else {
			System.out.println("4th digit");
		}

	}

}
