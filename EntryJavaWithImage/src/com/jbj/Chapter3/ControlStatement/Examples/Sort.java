package com.jbj.Chapter3.ControlStatement.Examples;

public class Sort {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a[] = { 210, 19, 72, 129, 34 };
		int l = a.length;
		int i, j, k;

		System.out.println("date status");
		for (i = 0; i < l; i++) {
			System.out.println(a[i] + " ");
		}
		System.out.println();

		for (j = 0; j < l - 1; j++) {
			for (i = j + 1; i < l; i++) {
				if (a[j] > a[i]) {
					k = a[j];
					a[j] = a[i];
					a[i] = k;
				}
			}
		}
		System.out.println("after arrange");
		for (i = 0; i < l; i++) {
			System.out.println(a[i] + " ");
		}
		System.out.println();
	}

}
