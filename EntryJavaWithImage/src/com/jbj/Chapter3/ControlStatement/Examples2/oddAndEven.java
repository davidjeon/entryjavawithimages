package com.jbj.Chapter3.ControlStatement.Examples2;

public class oddAndEven {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int oddSum = 0;
		int evenSum = 0;

		for (int i = 0; i <= 10; i++) {
			if ((i % 2) == 0) {
				evenSum += i;
				System.out.println(i);
				System.out.println("even");
				System.out.println();
			} else {
				oddSum += i;
				System.out.println(i);
				System.out.println("odd");
				System.out.println();
			}
			System.out.println(i);
			System.out.println();
		}

		System.out.println(evenSum);
		System.out.println(oddSum);
	}

}
