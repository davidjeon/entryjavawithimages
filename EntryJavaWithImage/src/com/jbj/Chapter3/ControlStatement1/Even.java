package com.jbj.Chapter3.ControlStatement1;

public class Even {
	public static void main(String[] args) {

		int a = 4;
		if (a % 2 == 0) {
			System.out.println(a + " is even");
		} else {
			System.out.println(a + " is odd");
		}

	}
}
