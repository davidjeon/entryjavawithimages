package com.jbj.Chapter2.calculation3;

public class Truth {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String right = "truth", wrong = "false";
		boolean value;

		// 조건이 참인 경우는 오른쪽을 선택
		value = true;
		String answer = value ? right : wrong;
		System.out.println(answer);

		// 조건이 거짓인 경우는 왼쪽을 선택
		value = false;
		answer = value ? right : wrong;
		System.out.println(answer);

	}

}
